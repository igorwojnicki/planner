<?php
/* 
   Copyright (C) 2020 - 2023  Igor Wojnicki

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

// it is based on https://tcpdf.org/ 

require_once('TCPDF/tcpdf_import.php');

class Planner extends TCPDF {

  private $workWidth, $workHeight, $leftMargin=20;
  private $lineHeight; 
  
  function __construct() {
    parent::__construct('P','mm',array(222,297)); // ReMarkable page size: 222x297mm
  }

  public function make($year,$lang) {
    $this->setPrintHeader(false);
    $this->setPrintFooter(false);
    $this->setMargins($this->leftMargin,0,0,0);
    $this->workWidth=$this->getPageWidth()-$this->leftMargin;
    $this->workHeight=$this->getPageHeight();
    $this->lineHeight=($this->workHeight-10)/28;
    $this->setAutoPageBreak(false);
    $interval1d=new DateInterval('P1D');
    $start=new DateTime($year.'-01-01');
    $end=new DateTime($year.'-12-31');
    $fmt = new IntlDateFormatter(
      $lang,
      IntlDateFormatter::FULL,
      IntlDateFormatter::FULL,
      NULL, // timezone
      IntlDateFormatter::GREGORIAN
    );
    $day=1;
    while ($start<=$end) {
      $dow=$day%7;
      if ($dow==1) {
        $fmt->setPattern('LLLL yyyy');
        $this->head($fmt->format($start)); 
      }
      if ($dow==$start->format('w')) {
        $fmt->setPattern('EEEE d');
        $content=$fmt->format($start->getTimestamp());
        $start->add($interval1d);
      } else {
        $content='';
      }
      $this->day($content,$dow);
      $day++;
    }
  }

  private function head($what) {
    $this->AddPage();
    $this->SetLineWidth(0.1);
    $this->SetFont('dejavusansb','',16);
    $this->Cell(0,10,$what,1,1,'C');
  }

  private function day($what,$whichDow) {
    static $lastx=0;
    switch ($whichDow) {
      case 0: //Sunday
        $this->setX($lastx); //position at the end of last weekday 
      case 6: //Saturday
        $this->SetFont('dejavusansb','',16);
        $this->hours($what,true);
        $this->SetLineWidth(0.8);
	$this->Cell(($this->workWidth)/3,($this->workHeight-10)/4,'',1,0,'L');
        break;
      default: //Weekday
        $this->hours($what);
        $this->SetFont('dejavusansb','',16);
        $this->SetLineWidth(0.8);
        $this->Cell(($this->workWidth)/3,($this->workHeight-10)/2,'',1,0,'L');
        $lastx=$this->getX();
    }
    if ($whichDow==3 || $whichDow==6) $this->Ln(); //start a new row
  }

  private function hours($what,$isShort=false) {
    $start=8;
    if ($isShort)
      $items=6;
    else
      $items=12;
    $tmpx=$this->getX();
    $tmpy=$this->getY();
    $this->setX($tmpx+($this->workWidth)/3/2);
    $this->Cell(10,$this->lineHeight,$what,0,0,'C');
    $this->setXY($tmpx,$this->getY()+$this->lineHeight);
    $this->SetFont('dejavusans','',16);
    $this->SetLineWidth(0.1);
    for ($i=$start; $i<=$start+$items; $i++){
      if ($isShort) {
	$this->Line($this->getX(),$this->getY(),$this->getX()+($this->workWidth)/3,$this->getY());
      }	else {
	$this->Cell(10,$this->lineHeight,$i,0,0,'L');
	$this->Line($this->getX(),$this->getY(),$this->getX()+(($this->workWidth)-30)/3,$this->getY());
      }
      $this->setXY($tmpx,$this->getY()+$this->lineHeight);
    }
    $this->setXY($tmpx,$tmpy);
  }
}

if (isset($argv[1])) {
  if ($argv[1] == '-h') {
    echo 'Usage: '.$argv[0].' [-h] [year [language]]'."\n";
    echo "Generate a PDF planner for a given year\nOptionally specify language (e.g. en_US, default: pl_PL).\n";
    exit(0);
  }
  $year=$argv[1];
} else {
  $year=date('Y');
}

if (isset($argv[2])) {
  $lang=$argv[2];
} else {
  $lang='pl_PL';
}

$p=new Planner();
$p->make($year,$lang);
$p->Output();
?>
