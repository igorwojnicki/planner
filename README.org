* Planner/calendar generator

A planner/calendar PDF generator: one page is one week. Designed esp. for Remarkable ([[http://www.remarkable.com]]) as a template.

Ready to use PDFs can be faound in the [[file:pdfs/][pdfs]] directory.


* Run

By running ~planner.php~ script you will get a planner PDF file for current year in Polish on the standard output.

If you need it for a specific year or language (e.g. 2023), specify them as the arguments:

#+begin_src sh
php planner.php 2023 en_US > my_planner_2023.pdf
#+end_src

* Acknowledgments

[[https://tcpdf.org/][TCPDF]] was downloaded from: [[https://github.com/tecnickcom/TCPDF]].

